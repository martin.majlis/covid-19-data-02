PYTHON=python3.7
GIT=git
TIME=/usr/bin/time

DIR_ROOT=./
DIR_DATA=$(DIR_ROOT)/data
DIR_DATA_SUPPORT=$(DIR_DATA)/support/
DIR_DATA_BACKUP=$(DIR_DATA)/backup/
DIR_DATA_DERIVED=$(DIR_DATA)/derived/

F_NUTS_RAW=$(DIR_DATA_SUPPORT)/nuts-raw.csv
F_NUTS_ENRICHED=$(DIR_DATA_SUPPORT)/nuts-enriched.csv

WEB_ONEMOCNENI_MZCR=https://onemocneni-aktualne.mzcr.cz/
DIR_MZCR_WEB=$(DIR_DATA_BACKUP)/onemocneni-aktualne.mzcr.cz_covid-19-web/
DIR_MZCR_V1=$(DIR_DATA_BACKUP)/onemocneni-aktualne.mzcr.cz_covid-19/
API_MZCR_V1=$(WEB_ONEMOCNENI_MZCR)api/v1/covid-19/
DIR_MZCR_V2=$(DIR_DATA_BACKUP)/onemocneni-aktualne.mzcr.cz_covid-19-v2/
API_MZCR_V2=$(WEB_ONEMOCNENI_MZCR)api/v2/covid-19/

WEB_DIP_MZCR=https://dip.mzcr.cz/
API_DIP_MZCR_V1=$(WEB_DIP_MZCR)api/v1/
DIR_DIP_MZCR_V1=$(DIR_DATA_BACKUP)/dip.mzcr.cz_v1/

WEB_UZIS=https://share.uzis.cz/s/
DIR_UZIS=$(DIR_DATA_BACKUP)/share.uzis.cz/

WEB_SUKL=https://www.sukl.cz/
DIR_SUKL=$(DIR_DATA_BACKUP)/sukl.cz/

DIR_CSSEGI=$(DIR_DATA_BACKUP)/CSSEGISandData_COVID-19/csse_covid_19_time_series/
API_CSSEGI=https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/

DIR_DERIVED_MZCR=$(DIR_DATA_DERIVED)/onemocneni-aktualne.mzcr.cz_covid-19/

install: install-python-requirements

check-mypy:
	mypy scripts --ignore-missing-imports

download_p=$(PYTHON) -c "from scripts.utils import download; download('$1', '$2', 0); import time; time.sleep(2);"

download=curl -L -o "$2"  "$1"; $(GIT) add "$2"; $(GIT) commit -m "Download $1" "$2"; sleep 2;

install-python-requirements:
	$(PYTHON) -m pip install -r scripts/requirements.txt


nuts-enrichment:
	cat $(F_NUTS_RAW) | $(PYTHON) ./scripts/nuts_enrichment.py | tee $(F_NUTS_ENRICHED)

download: download-onemocneni-aktualne.mzcr.cz download-CSSEGI download-share.uzis.cz download-dip.mzcr.cz download-sukl.cz

download-onemocneni-aktualne.mzcr.cz: download-onemocneni-aktualne.mzcr.cz_covid-19-v2 download-onemocneni-aktualne.mzcr.cz_covid-19-web

download-onemocneni-aktualne.mzcr.cz_covid-19-web:
	mkdir -p ${DIR_MZCR_WEB}
	$(call download,$(WEB_ONEMOCNENI_MZCR)covid-19,$(DIR_MZCR_WEB)covid-19.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)kapacity-luzkove-pece,$(DIR_MZCR_WEB)covid-19-kapacity-luzkove-pece.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)vyvoj-kapacit-luzkove-pece,$(DIR_MZCR_WEB)vyvoj-kapacit-luzkove-pece.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)pes,$(DIR_MZCR_WEB)covid-19-pes.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)aktualni-situace-cr,$(DIR_MZCR_WEB)covid-19-aktualni-situace-cr.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)aktualni-situace-pha,$(DIR_MZCR_WEB)covid-19-aktualni-situace-pha.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)aktualni-situace-stc,$(DIR_MZCR_WEB)covid-19-aktualni-situace-stc.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)aktualni-situace-jhc,$(DIR_MZCR_WEB)covid-19-aktualni-situace-jhc.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)aktualni-situace-plk,$(DIR_MZCR_WEB)covid-19-aktualni-situace-plk.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)aktualni-situace-kvk,$(DIR_MZCR_WEB)covid-19-aktualni-situace-kvk.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)aktualni-situace-ulk,$(DIR_MZCR_WEB)covid-19-aktualni-situace-ulk.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)aktualni-situace-hkk,$(DIR_MZCR_WEB)covid-19-aktualni-situace-hkk.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)aktualni-situace-pak,$(DIR_MZCR_WEB)covid-19-aktualni-situace-pak.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)aktualni-situace-vys,$(DIR_MZCR_WEB)covid-19-aktualni-situace-vys.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)aktualni-situace-jhm,$(DIR_MZCR_WEB)covid-19-aktualni-situace-jhm.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)aktualni-situace-olk,$(DIR_MZCR_WEB)covid-19-aktualni-situace-olk.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)aktualni-situace-zlk,$(DIR_MZCR_WEB)covid-19-aktualni-situace-zlk.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)aktualni-situace-msk,$(DIR_MZCR_WEB)covid-19-aktualni-situace-msk.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)covid-19/kraje/PHA,$(DIR_MZCR_WEB)covid-19-kraje-PHA.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)covid-19/kraje/STC,$(DIR_MZCR_WEB)covid-19-krajec-STC.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)covid-19/kraje/JHC,$(DIR_MZCR_WEB)covid-19-kraje-JHC.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)covid-19/kraje/PLK,$(DIR_MZCR_WEB)covid-19-kraje-PLK.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)covid-19/kraje/KVK,$(DIR_MZCR_WEB)covid-19-kraje-KVK.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)covid-19/kraje/ULK,$(DIR_MZCR_WEB)covid-19-kraje-ULK.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)covid-19/kraje/LBK,$(DIR_MZCR_WEB)covid-19-kraje-LBK.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)covid-19/kraje/HKK,$(DIR_MZCR_WEB)covid-19-kraje-HKK.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)covid-19/kraje/PAK,$(DIR_MZCR_WEB)covid-19-kraje-PAK.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)covid-19/kraje/VYS,$(DIR_MZCR_WEB)covid-19-kraje-VYS.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)covid-19/kraje/JHM,$(DIR_MZCR_WEB)covid-19-kraje-JHM.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)covid-19/kraje/OLK,$(DIR_MZCR_WEB)covid-19-kraje-OLK.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)covid-19/kraje/ZLK,$(DIR_MZCR_WEB)covid-19-kraje-ZLK.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)covid-19/kraje/MSK,$(DIR_MZCR_WEB)covid-19-kraje-MSK.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)covid-19/kumulativni-prehledy,$(DIR_MZCR_WEB)covid-19-kumulativni-prehledy.html)
	$(call download,$(WEB_ONEMOCNENI_MZCR)covid-19/prehledy-khs,$(DIR_MZCR_WEB)covid-19-prehledy-khs.html)

download-onemocneni-aktualne.mzcr.cz_covid-19-v2:
	mkdir -p ${DIR_MZCR_V2}
	$(call download,$(API_MZCR_V2)covid-19,$(DIR_MZCR_V2)covid-19.html)
	$(call download,$(API_MZCR_V2)testy.csv,$(DIR_MZCR_V2)testy.csv)
	$(call download,$(API_MZCR_V2)testy-pcr-antigenni.csv,$(DIR_MZCR_V2)testy-pcr-antigenni.csv)
	$(call download,$(API_MZCR_V2)kraj-okres-testy.csv,$(DIR_MZCR_V2)kraj-okres-testy.csv)
	$(call download,$(API_MZCR_V2)nakaza.csv,$(DIR_MZCR_V2)nakaza.csv)
	$(call download,$(API_MZCR_V2)nakazeni-vyleceni-umrti-testy.csv,$(DIR_MZCR_V2)nakazeni-vyleceni-umrti-testy.csv)
	$(call download,$(API_MZCR_V2)vyleceni.csv,$(DIR_MZCR_V2)vyleceni.csv)
	$(call download,$(API_MZCR_V2)umrti.csv,$(DIR_MZCR_V2)umrti.csv)
	$(call download,$(API_MZCR_V2)osoby.csv,$(DIR_MZCR_V2)osoby.csv)
	$(call download,$(API_MZCR_V2)kraj-okres-nakazeni-vyleceni-umrti.csv,$(DIR_MZCR_V2)kraj-okres-nakazeni-vyleceni-umrti.csv)
	$(call download,$(API_MZCR_V2)pomucky.csv,$(DIR_MZCR_V2)pomucky.csv)
	$(call download,$(API_MZCR_V2)zakladni-prehled.csv,$(DIR_MZCR_V2)zakladni-prehled.csv)
	$(call download,$(API_MZCR_V2)orp.csv,$(DIR_MZCR_V2)orp.csv)
	# $(call download,$(API_MZCR_V2)obce.csv,$(DIR_MZCR_V2)obce.csv)
	$(call download,$(API_MZCR_V2)mestske-casti.csv,$(DIR_MZCR_V2)mestske-casti.csv)
	$(call download,$(API_MZCR_V2)hospitalizace.csv,$(DIR_MZCR_V2)hospitalizace.csv)
	$(call download,$(API_MZCR_V2)ockovani.csv,$(DIR_MZCR_V2)ockovani.csv)
	# $(call download,$(API_MZCR_V2)ockovaci-mista.csv,$(DIR_MZCR_V2)ockovaci-mista.csv)
	$(call download,$(API_MZCR_V2)prehled-ockovacich-mist.csv,$(DIR_MZCR_V2)prehled-ockovacich-mist.csv)
	$(call download,$(API_MZCR_V2)prehled-odberovych-mist.csv,$(DIR_MZCR_V2)prehled-odberovych-mist.csv)
	$(call download,$(API_MZCR_V2)ockovani-spotreba.csv,$(DIR_MZCR_V2)ockovani-spotreba.csv)
	$(call download,$(API_MZCR_V2)ockovani-distribuce.csv,$(DIR_MZCR_V2)ockovani-distribuce.csv)
	$(call download,$(API_MZCR_V2)kapacity-intenzivni-pece-vlna-2.csv,$(DIR_MZCR_V2)kapacity-intenzivni-pece-vlna-2.csv)

download-share.uzis.cz:
	mkdir -p ${DIR_UZIS}
	# links from https://onemocneni-aktualne.mzcr.cz/pes - Data ke stazeni
	$(call download,$(WEB_UZIS)BRfppYFpNTddAy4/download?path=%2F&files=pes_CR_verze2.csv,$(DIR_UZIS)pes_CR_verze2.csv)
	$(call download,$(WEB_UZIS)BRfppYFpNTddAy4/download?path=%2F&files=pes_kraje_verze2.csv,$(DIR_UZIS)pes_kraje_verze2.csv)
	$(call download,$(WEB_UZIS)BRfppYFpNTddAy4/download?path=%2F&files=pes_okresy_verze2.csv,$(DIR_UZIS)pes_okresy_verze2.csv)
	$(call download,$(WEB_UZIS)BRfppYFpNTddAy4/download?path=%2F&files=pes_data_readme.txt,$(DIR_UZIS)pes_data_readme.txt)

	$(call download,$(WEB_UZIS)dCZBiARJ27ayeoS/download?path=%2F&files=mestske-casti.csv,$(DIR_UZIS)mestske-casti.csv)
	$(call download,$(WEB_UZIS)dCZBiARJ27ayeoS/download?path=%2F&files=obec.csv,$(DIR_UZIS)obec.csv)

download-dip.mzcr.cz:
	mkdir -p $(DIR_DIP_MZCR_V1)
	$(call download,$(API_DIP_MZCR_V1)kapacity-intenzivni-pece-vlna-2.csv,$(DIR_DIP_MZCR_V1)kapacity-intenzivni-pece-vlna-2.csv)
	# link from https://onemocneni-aktualne.mzcr.cz/vyvoj-kapacit-luzkove-pece
	$(call download,$(API_DIP_MZCR_V1)kapacity-intenzivni-pece-zdravotnicke-zarizeni.csv,$(DIR_DIP_MZCR_V1)kapacity-intenzivni-pece-zdravotnicke-zarizeni.csv)

download-sukl.cz:
	mkdir -p $(DIR_SUKL)
	$(call download,$(WEB_SUKL)covid-19,$(DIR_SUKL)covid-19.html)
	$(call download,$(WEB_SUKL)tydenni-zpravy-o-prijatych-hlasenich-podezreni-na-nezadouci,$(DIR_SUKL)tydenni-nezadouci-ucinky.html)
	$(call download,$(WEB_SUKL)informace-pro-zdravotnicke-pracovniky-k-vakcinam-proti-covid,$(DIR_SUKL)vakciny-info.html)
	$(call download,$(WEB_SUKL)pribalove-informace-k-registrovanym-vakcinam-proti-covid-19,$(DIR_SUKL)vakciny-pribalove-info.html)
	$(call download,$(WEB_SUKL)sukl/comirnaty-prehledne-dulezita-fakta-o-prvni-podminecne,$(DIR_SUKL)vakcina-comirnaty.html)
	$(call download,$(WEB_SUKL)sukl/covid-19-vaccine-moderna-otazky-a-odpovedi,$(DIR_SUKL)vakcina-moderna.html)
	$(call download,$(WEB_SUKL)sukl/covid-19-vaccine-astrazeneca-prehledne-jak-ucinkuje-a-pro,$(DIR_SUKL)vakcina-astrazeneca.html)

sort: sort-onemocneni-aktualne.mzcr.cz_covid-19-v2

sort-onemocneni-aktualne.mzcr.cz_covid-19-v2:
	head -n1 $(DIR_MZCR_V2)osoby.csv > $(DIR_MZCR_V2)osoby-sorted.csv
	tail -n +2 $(DIR_MZCR_V2)osoby.csv | sort >> $(DIR_MZCR_V2)osoby-sorted.csv

download-CSSEGI:
	mkdir -p $(DIR_CSSEGI)
	$(call download,$(API_CSSEGI)time_series_covid19_confirmed_global.csv,$(DIR_CSSEGI)time_series_covid19_confirmed_global.csv)
	$(call download,$(API_CSSEGI)time_series_covid19_deaths_global.csv,$(DIR_CSSEGI)time_series_covid19_deaths_global.csv)
	$(call download,$(API_CSSEGI)time_series_covid19_recovered_global.csv,$(DIR_CSSEGI)time_series_covid19_recovered_global.csv)

transform-CSSEGISandData-COVID:
	$(PYTHON) ./scripts/transform-CSSEGISandData-COVID-19.py

transform-oa-mzcr-covid: transform-oa-mzcr-covid-web

transform-oa-mzcr-covid-pomucky:
	mkdir -p $(DIR_DERIVED_MZCR)
	$(PYTHON) ./scripts/transform_oa_mzcr_covid.py $(DIR_ROOT) $(DIR_MZCR_V1) $(DIR_DERIVED_MZCR)

transform-oa-mzcr-covid-web:
	mkdir -p $(DIR_DERIVED_MZCR)
	cat $(DIR_MZCR_V1)covid-19.html | \
	tr '\n' ' ' | \
	sed -r 's/^\s+//g;s/(\[|\]|\}|\{)/\n\1\n/g;s/>/>\n/g;s/</\n</g;s/^\s+//g' > $(DIR_DERIVED_MZCR)covid-19.html

# transform: transform-CSSEGISandData-COVID transform-oa-mzcr-covid
transform: transform-oa-mzcr-covid

step-pull:
	echo "PULL - "`date` && \
	$(GIT) pull

step-download-data:
	echo "DOWNLOADING - "`date`; \
	$(MAKE) download; \
	$(GIT) add $(DIR_DATA_BACKUP); \
	$(GIT) status; \
	$(GIT) commit -q -m "Automatic data update - download - $(shell date --rfc-3339=seconds -u)"; \
	$(TIME) -v $(GIT) push -v;

step-sort-data:
	echo "SORTING - "`date` && \
	$(MAKE) sort && \
	$(GIT) status && \
	$(GIT) add $(DIR_DATA); \
	$(GIT) commit -q -m "Automatic data update - sort - $(shell date --rfc-3339=seconds -u)" && \
	free && \
	$(TIME) -v $(GIT) push -v

step-transform-data:
	echo "TRANSFORMING - "`date` && \
	$(MAKE) transform && \
	$(GIT) status && \
	$(GIT) add $(DIR_DATA); \
	$(GIT) commit -q -m "Automatic data update - transform - $(shell date --rfc-3339=seconds -u)" && \
	free && \
	$(TIME) -v $(GIT) push -v

update-data: step-pull step-download-data step-sort-data step-transform-data
