#!/usr/bin/env python3

# Adds extra columns into

import csv
import sys
import time

from geopy.geocoders import Nominatim

# https://www.czso.cz/csu/czso/cri/vyvoj-obyvatelstva-v-krajich-ceske-republiky-2019
NUTS3_POPULATION = {
    'CZ': 10693939,
    'CZ0': 10693939,
    'CZ010': 1324277,
    'CZ020': 1385141,
    'CZ031': 644083,
    'CZ032': 589899,
    'CZ041': 294664,
    'CZ042': 820965,
    'CZ051': 443690,
    'CZ052': 551647,
    'CZ053': 522662,
    'CZ063': 509813,
    'CZ064': 1191981,
    'CZ071': 632015,
    'CZ072': 582555,
    'CZ080': 1200539,
}

if __name__ == "__main__":
    geolocator = Nominatim(
        user_agent="https://github.com/martin-majlis/covid-19-data"
    )

    reader = csv.reader(sys.stdin.readlines())
    writer = csv.writer(sys.stdout)

    COLUMN_CODE = 0
    COLUMN_NUTS3_NAME = 4

    # copy header and add two more columns
    header = next(reader)
    writer.writerow(header + ['Latitude', 'Longitude', 'Population'])
    for line in reader:
        extra = ["", "", ""]
        if line[COLUMN_NUTS3_NAME]:
            location = geolocator.geocode(line[COLUMN_NUTS3_NAME])
            if location:
                extra[0] = str(location.latitude)
                extra[1] = str(location.longitude)

            time.sleep(1)
        extra[2] = str(NUTS3_POPULATION.get(line[COLUMN_CODE], 0))
        print(f"{line[COLUMN_CODE]} - loc: {extra[0:2]}; pop: {extra[2]}", file=sys.stderr)

        writer.writerow(line + extra)
